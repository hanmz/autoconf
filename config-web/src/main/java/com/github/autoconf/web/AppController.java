package com.github.autoconf.web;

import com.github.autoconf.entity.ConfigHistory;
import com.github.autoconf.service.ConfigHistoryService;
import com.github.autoconf.service.ConfigService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 默认页面
 * Created by lirui on 2014/10/13.
 */
@Controller
public class AppController {
  @Autowired
  private ConfigHistoryService historyService;
  @Autowired
  private ConfigService configService;
  @Value("${casServerUrlPrefix}")
  private String casServerUrlPrefix = "/cas";

  @RequestMapping(value = {"/", "/home"})
  public String home() {
    return "home";
  }

  @RequestMapping("/logout")
  public String logout(RedirectAttributes r) {
    SecurityUtils.getSubject().logout();
    r.addFlashAttribute("message", "您已经安全退出");
    return "redirect:" + casServerUrlPrefix + "/logout";
  }

  @RequestMapping("/unauthorized")
  public String unauthorized() {
    return "unauthorized";
  }

  @RequestMapping("/history")
  public String recent(HttpServletRequest request, Model model) {
    String ajax = "ajax/history/";
    if (request.getQueryString() != null) {
      ajax += '?' + request.getQueryString();
    }
    model.addAttribute("ajax", ajax);
    return "history";
  }

  @RequestMapping("/view/history/{id}")
  public String viewHistory(@PathVariable long id, Model model) {
    ConfigHistory h = historyService.findbyId(id);
    model.addAttribute("history", h);
    model.addAttribute("configExist", configService.findById(h.getConfigId()) != null);
    return "view_history";
  }
}
