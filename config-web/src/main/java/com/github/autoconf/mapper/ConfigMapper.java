package com.github.autoconf.mapper;

import com.github.autoconf.entity.Config;
import com.github.mybatis.mapper.ICrudMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 访问config对象
 *
 * Created by lirui on 2015/1/10.
 */
public interface ConfigMapper extends ICrudMapper<Config> {
  @Update("UPDATE config SET path=#{path} WHERE id=#{id}")
  void updatePath(@Param("path") String path, @Param("id") Long id);
}
